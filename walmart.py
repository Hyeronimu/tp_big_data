# coding: utf-8

import pyspark
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, when, count, month, year
from pyspark.sql.types import IntegerType, FloatType
import sys


#1- Start a simple Spark Session
#commande spark-submit walmart.py, ou pyspark, et rentrer les instructions une à une


#2- Load the Walmart Stock CSV File
spark = SparkSession.builder.appName("Walmart Socket").getOrCreate()

#3- What are the columns names ?
# option("header", True) pour afficher les noms des colonnes
df = spark.read.option("header", True).csv("./../Data/walmart_stock.csv")

#4- What does the Shcema look like ?
#df.show() affiche les 20 premieres lignes
df.printSchema()


#5- Create a new dataframe with a comuln called HV_Ratio that is the ratio of the High Price versus volume of stock traded for a day
df2 = df.withColumn("HV_Ratio", col("High")/col("Volume")) # Cree nouvelle colonne a partir de df
df2.show()

#6- What day had the peak High in Price ?

# Transformation du dataframe en table pour utiliser les fonctions orderBy, etc ...
df.createOrReplaceTempView("dftable")

# Methode DSL
res = df.orderBy(col("High").desc()).select("Date").head()
print(res)

# Methode SQL
spark.sql("select Date From dftable where High = (select max(High) from dftable)").show()

#7- What is the mean of the Close column ?
# Close et Volume ici sont de type string, donc on les cast en Int !

df = df.withColumn("Close", col("Close").cast(FloatType()))

df.agg({"Close" : "mean"}).show() # DSL
# On remet a jour la table de df
df.createOrReplaceTempView("dftable")
spark.sql("select mean(Close) from dftable").show() # SQL

#8 What is the max and min of the Volume column ?
df = df.withColumn("Volume", col("Volume").cast(IntegerType()))
# On remet a jour la table de df
df.createOrReplaceTempView("dftable")
df.agg({"Volume" : "max"}).show()#DSL
spark.sql("select max(Volume) from dftable").show() #SQL

df.agg({"Volume" : "min"}).show()#DSL
spark.sql("select min(Volume) from dftable").show() # SQL


df = df.withColumn("High", col("High").cast(FloatType()))
df = df.withColumn("Low", col("Low").cast(FloatType()))

df.agg({"Volume" : "max"}).show()
spark.sql("select max(Volume) from dftable").show() # DSL et SQL

df.agg({"Volume" : "min"}).show()
spark.sql("select min(Volume) from dftable").show() # DSL et SQL


#9- How many days was the Close lower than 60 dollars ? 
nbjours = df.filter(df.Close < 60).count()
print(nbjours) # DSL

#10- What pourcentage of the time was the High greater than 80 ?<=> (Days High > 80 / Total Days)*100
pourcent = (df.filter(df.High > 80).count() / float(df.count()))*100  # float() sinon division entre deux entiers = 0 pour Python
print(pourcent)


#11- What is the max High per year ?
df.groupBy(year("Date").alias("Annee")).agg({"High" : "max"}).orderBy(col("Annee").desc()).show()


#12- What is the Average Close for each calendar Month ?
df.groupBy(month("Date").alias("Mois")).agg({"Close" : "avg"}).orderBy(col("Mois").asc()).show()
